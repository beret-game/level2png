#!/usr/bin/python
import argparse, Image, os, random, string, struct, sys

LVLMAX = 500
ROOMMAX = 100
SPR_SIZE = 30
THINGMAX = 250
THINGSIZE = 184

# enum types
(NOTYPE, BERET, WOODBLOCK, ICEBLOCK, STONEBLOCK, SMWOODBLOCK, SMICEBLOCK, SMSTONEBLOCK, WHITEDIE, BLACKDIE, BIGBLOCK, GRAVITYBLOCK, ANNOYINGBLOCK, TELEBLOCK, LINKBLOCK, INFECTLING, WEIGHT, BOMB, STICKYBOMB, DOOR, FINISHDOOR, SIGN, READSIGN, MEDALFRAGMENT, MEDALCORNER, WHITEMEDAL, TELESEEKER, ROBOT, SPIKEBALL, HOPPER, CARRIER, VSIGN, SOLIDSWITCH, SOLIDBLOCK, PLATFORM, FLOATBLOCK, GHOSTBLOCK, FAKEBLOCK, AURADROP, STONEY, GRAVITYSWITCH, TURRET, GHOST, REINCARNATOR, ANTIMATTER, FAKEBOMB, BLOCKSTER, MATTERLY, ANTISEEKER, ANTIFECTLING, TOPHAT, SHIELDGEN, TYPEMAX,BOMBHELPER, AURAHELPER, FIREBALL, SPIKEBLOCK, MATTERFRAG, TOPHATSHIELD) = range(59)

# enum direction
(UP, RIGHT, DOWN, LEFT, IN, SHIFT, NONE) = range(7)

# enum fronttype
(NOTSOLIDB, SOLID, SOLIDF, BERETONLYF, OBJONLYF, NOTSOLIDF) = range(-1, 5)

# enum bordertype
(U, R, D, L, UR, DR, DL, UL, URDOT, DRDOT, DLDOT, ULDOT, UD, LR, NU, NR, ND, NL, ALL, DLWDOT, ULWDOT, URWDOT, DRWDOT, UWLDOT, UWRDOT, RWUDOT, RWDDOT, DWLDOT, DWRDOT, LWUDOT, LWDDOT, UW2DOTS, RW2DOTS, DW2DOTS, LW2DOTS, ALLDOTS, NULDOT, NURDOT, NDRDOT, NDLDOT, UDOTS, RDOTS, DDOTS, LDOTS, ULDRDOTS, URDLDOTS, BLANK) = range(47)

# enum filltype
(EMPTY, WHITEFUZZ, ORANGEFUZZ, BLUEFUZZ, GREENFUZZ, REDFUZZ, WHITEFUZZSEE, ORANGEFUZZSEE, BLUEFUZZSEE, GREENFUZZSEE, OBJONLY, BERETONLY, CHOICEONLY, GLASS, PURPLETILE, REDTILE, GREENTILE, WHITETILE, PURPLETILESEE, REDTILESEE, GREENTILESEE, WHITETILESEE, CLEAR, HORIZWOOD, VERTWOOD, SPIKEU, SPIKER, SPIKED, SPIKEL, REDFUZZSEE, WHITESPIRAL, GREENSPIRAL, BLUESPIRAL, REDSPIRAL, PURPLESPIRAL, WHITESPRSEE, GREENSPRSEE, BLUESPRSEE, REDSPRSEE, PURPLESPRSEE, MOVERU, MOVERR, MOVERD, MOVERL, REDCARPET, GREENCARPET, PURPLECARPET, ORANGECARPET, BLUECARPET, REDSEECRPT, GREENSEECRPT, PURPLESEECRPT, ORANGESEECRPT, BLUESEECRPT, BLACKTILE, BLACKTILESEE, ANTITILE) = range(57)
MAPTILE=73
TILEMAX=93

beret = {}
istophat=0
opt = {
    'drawBeret': True,
}

def checkint(int):
    if int == 4294967295: return 0
    return int

def drawSprite(x, y, sprx, spry, sprw, sprh, source, dest):
    imagecopy(dest, source, int(x), int(y), int(sprx * SPR_SIZE), int(spry * SPR_SIZE), int(sprw * SPR_SIZE), int(sprh * SPR_SIZE))

def drawSprites(front):
    args = 6 * [None]
    for i in range(THINGMAX):
        if lv_things[i]['type'] != NOTYPE and ((lv_things[i]['infront'] == front and not lv_things[i]['inback']) or (lv_things[i]['inback'] and front == -1)):
            drawThing(lv_things[i], args)
            if (args[2] > -1):
                drawSprite(args[0], args[1], args[2], args[3], args[4], args[5], spritesheet, lv_img)

def drawThing(thing, args):
    args[0] = thing['x']
    args[1] = thing['y']
    args[4] = 1
    args[5] = 1

    if thing['type'] == GRAVITYSWITCH: # 40
        args[2] = 6
        args[3] = 12 + 4 * thing['subtype'] + thing['anim']
    elif thing['type'] == TURRET: # 41
        args[2] = 10 + thing['anim']
        args[3] = 13 - thing['dir']
    elif thing['type'] == FIREBALL: # 55
        args[2] = 11
        args[3] = 5
    elif thing['type'] == BLOCKSTER: # 46
        args[2] = 18
        args[3] = 10 + int(thing['anim'])
    elif thing['type'] == SPIKEBLOCK: # 56
        args[2] = 18
        args[3] = thing['subtype']
    elif thing['type'] == STONEY: # 39
        if thing['subtype'] == 2:
            args[2] = 16 + thing['dir']
            args[3] = 9
        else:
            args[2] = 16 + thing['anim']
            args[3] = (8, 6 + thing['dir'])[thing['subtype'] > 0]
    elif thing['type'] == SOLIDSWITCH: # 32
        args[2] = 6 + thing['subtype']
        args[3] = thing['anim']
    elif thing['type'] == SOLIDBLOCK: # 33
        args[2] = 4 + thing['subtype'] / 2
        args[3] = thing['anim']
    elif thing['type'] == PLATFORM: # 34
        if (thing['subtype'] == 0 and thing['starty'] - thing['timer'] + 30 <= thing['y']) or (thing['subtype'] == 1 and thing['starty'] + thing['timer'] - 30 >= thing['y']):
            args[1] += random.randint(-1,1)
        elif (thing['subtype'] == 2 and thing['startx'] - thing['timer'] - 30 >= thing['x']) or (thing['subtype'] == 3 and thing['startx'] - thing['timer'] + 30 <= thing['x']):
            args[0] += random.randint(-1,1)
        args[2] = 3
        args[3] = 12 + thing['subtype']
        args[4] = (1, 2)[thing['subtype'] > 1]
    elif thing['type'] == BIGBLOCK: # 10
        args[2] = (3,5)[thing['subtype'] == 3]
        args[3] = (4 + thing['subtype'], 6)[thing['subtype'] == 3]
        if thing['subtype'] > 1:
            args[4] = 2
            args[5] = 2
    elif thing['type'] == MEDALFRAGMENT: # 23
        if thing['subtype'] < 2:
            args[2] = 2
            args[3] = 6 + thing['subtype']
        else:
            args[2] = 8
            args[3] = 4 + thing['subtype']
    elif thing['type'] == MEDALCORNER: # 24
        if thing['subtype'] < 4:
            args[2] = (3, 4)[thing['subtype'] < 3]
            args[3] = (10, 8 + thing['subtype'])[thing['subtype'] < 3]
        elif thing['subtype'] == 4:
            args[2] = 15
            args[3] = 15
        elif thing['subtype'] == 5:
            args[2] = 17
            args[3] = 16
        elif thing['subtype'] == 6:
            args[2] = 16
            args[3] = 17
        elif thing['subtype'] == 7:
            args[2] = 16
            args[3] = 16
    elif thing['type'] == WHITEMEDAL: # 25
        if thing['subtype'] == 0:
            args[2] = 5
            args[3] = 15
        else:
            args[2] = 15
            args[3] = 14
    elif thing['type'] == ANTIMATTER: # 44
        args[0] -= 2
        args[2] = 15
        args[3] = 11
    elif thing['type'] == FAKEBOMB: # 45
        if int(thing['anim']) == 0:
            args[2] = 2
            args[3] = 2 + thing['subtype']
        else:
            args[2] = 15 + int(thing['anim'])
            args[3] = 10 + thing['subtype'] + thing['dir'] * 3
    elif thing['type'] == AURADROP: # 38
        args[2] = 9 + thing['dir'] * 3 + thing['anim']
        if int(thing['anim']) == 3:
            args[2] -= 2
        args[3] = 11
    elif thing['type'] == TOPHATSHIELD: # 58
        args[2] = 12
        args[3] = 14
        args[4] = 3
        args[5] = 3
    elif thing['type'] == AURAHELPER: # 54
        args[2] = 11
        args[3] = 6
        args[4] = 5
        args[5] = 5
    elif thing['type'] == BOMBHELPER: # 53
        args[2] = -1
    elif thing['type'] == GHOST: # 42
        if thing['anim'] >= 3:
            args[2] = -1
        else:
            args[2] = 8 + thing['dir']
            args[3] = 14 + thing['anim']
    elif thing['type'] == REINCARNATOR: # 43
        args[2] = 8
        args[3] = 3 * thing['subtype']
        args[5] = 3
    elif thing['type'] == BERET: # 1
        if thing['subtype'] == 0:
            args[3] = int(thing['anim']) % 8
            if thing['jump']:
                args[3] = (9, 8)[thing['vy'] <= 0]
            args[0] = thing['x'] - 6 + thing['dir']
            args[1] = thing['y'] - 6
            args[2] = 1 - thing['dir']
    elif thing['type'] == TOPHAT: # 50
        if not thing['jump']:
            args[2] = 7 + int(thing['anim']) % 8
            args[3] = 18 + thing['dir']
        else:
            args[2] = 15
            args[3] = (18,16)[thing['vy'] <= 0] + thing['dir']
        args[0] = thing['x'] - 6 + thing['dir']
        args[1] = thing['y'] - 6
    elif thing['type'] == SHIELDGEN: # 51
        args[0] = thing['x'] - 5
        args[1] = thing['y'] - 5
        args[3] = 17
        if int(thing['anim']) % 4 == 0:
            args[2] = 9
        elif int(thing['anim']) == 1 or int(thing['anim']) == 3:
            args[2] = 11
        elif int(thing['anim']) == 2:
            args[2] = 12
        elif int(thing['anim']) == 5 or int(thing['anim']) == 7:
            args[2] = 13
        else:
            args[2] = 14
    elif thing['type'] == WOODBLOCK: # 2
        args[2] = (thing['type'] - WOODBLOCK) % 3
        args[3] = 10 + thing['subtype'] + (4, 0)[thing['type'] < SMWOODBLOCK]
    elif thing['type'] == SMWOODBLOCK: # 5
        args[2] = (thing['type'] - WOODBLOCK) % 3
        args[3] = 10 + thing['subtype'] + (4, 0)[thing['type'] < SMWOODBLOCK]
    elif thing['type'] == ICEBLOCK: # 3
        args[2] = (thing['type'] - WOODBLOCK) % 3
        args[3] = 10 + thing['subtype'] + (4, 0)[thing['type'] < SMWOODBLOCK]
    elif thing['type'] == SMICEBLOCK: # 6
        args[2] = (thing['type'] - WOODBLOCK) % 3
        args[3] = 10 + thing['subtype'] + (4, 0)[thing['type'] < SMWOODBLOCK]
    elif thing['type'] == STONEBLOCK: # 4
        args[2] = (thing['type'] - WOODBLOCK) % 3
        args[3] = 10 + thing['subtype'] + (4, 0)[thing['type'] < SMWOODBLOCK]
    elif thing['type'] == SMSTONEBLOCK: # 7
        args[2] = (thing['type'] - WOODBLOCK) % 3
        args[3] = 10 + thing['subtype'] + (4, 0)[thing['type'] < SMWOODBLOCK]
    elif thing['type'] == FAKEBLOCK: # 37
        if thing['anim'] < 1:
            args[2] = thing['subtype']
            args[3] = 10
        else:
            args[2] = 12 + thing['subtype'] + thing['dir'] * 3
            args[3] = 2 + thing['anim']
    elif thing['type'] == GHOSTBLOCK: # 36
        args[2] = 6
        args[3] = 9 + thing['anim']
    elif thing['type'] == WEIGHT: # 16
        args[0] -= 2
        args[2] = 4
        args[3] = 13
    elif thing['type'] == BOMB: # 17
        args[2] = 2
        args[3] = 2 + thing['subtype']
    elif thing['type'] == SIGN: # 21
        args[2] = 7 + thing['subtype'] / 2
        args[3] = 10 + thing['subtype'] % 2
    elif thing['type'] == VSIGN: # 31
        args[2] = 7
        args[3] = 13 + thing['subtype']
    elif thing['type'] == READSIGN: # 22
        args[2] = 7
        args[3] = 12
    elif thing['type'] == DOOR: # 19
        if thing['subtype'] < 2:
            args[2] = 2
            args[3] = thing['subtype']
        else:
            args[2] = 8
            args[3] = 12
            args[4] = 2
            args[5] = 2
    elif thing['type'] == FINISHDOOR: # 20
        args[2] = 7
        args[3] = 8
        args[4] = 2
        args[5] = 2
    elif thing['type'] == STICKYBOMB: # 18
        if thing['status'] == 0:
            args[2] = 2
            args[3] = 5
        else:
            if thing['timer'] % (5, 30)[thing['timer'] < 300] < 2:
                args[2] = 4 + (thing['status'] == CRASHD or thing['status'] == CRASHL)
                args[3] = 11 + (thing['status'] == CRASHU or thing['status'] == CRASHL)
            else:
                args[2] = 2 + (thing['status'] == CRASHD or thing['status'] == CRASHL)
                args[3] = 8 + (thing['status'] == CRASHU or thing['status'] == CRASHL)
    elif thing['type'] == WHITEDIE: # 8
        args[2] = thing['subtype'] % 3
        args[3] = 16 + 2 * (thing['type'] - WHITEDIE) + thing['subtype'] / 3
    elif thing['type'] == BLACKDIE: # 9
        args[2] = thing['subtype'] % 3
        args[3] = 16 + 2 * (thing['type'] - WHITEDIE) + thing['subtype'] / 3
    elif thing['type'] == FLOATBLOCK: # 35
        args[2] = 6
        args[3] = 8
    elif thing['type'] == GRAVITYBLOCK: # 11
        args[2] = 3 + thing['subtype'] % 2
        args[3] = 16 + thing['subtype'] / 2
    elif thing['type'] == ANNOYINGBLOCK: # 12
        args[2] = 7
        args[3] = 6 + thing['subtype']
    elif thing['type'] == TELEBLOCK: # 13
        args[2] = 5
        args[3] = 10 - thing['status'] / 2
    elif thing['type'] == LINKBLOCK: # 14
        args[2] = 3 + thing['subtype'] % 2
        args[3] = 18 + thing['subtype'] / 2
    elif thing['type'] == ANTISEEKER: # 48
        args[2] = 10 + thing['dir']
        args[3] = 16
    elif thing['type'] == MATTERFRAG: # 57
        args[0] = thing['x'] - 1
        args[1] = thing['y'] - 1
        if thing['subtype'] < 2:
            args[2] = 10 + thing['subtype']
            args[3] = 15
        else:
            args[2] = 10
            args[3] = 17
    elif thing['type'] == MATTERLY: # 47
        args[2] = 10 + thing['dir']
        args[3] = 14
    elif thing['type'] == TELESEEKER: # 26
        args[2] = 9 + thing['dir']
        if thing['subtype'] == 2:
            args[3] = 9
        elif thing['subtype'] == 3:
            args[3] = 10
        else:
            args[3] = (3, 6)[thing['subtype'] == 1] + thing['anim']
    elif thing['type'] == ROBOT: # 27
        args[2] = 9 + thing['dir'] + 3 * thing['subtype']
        args[3] = thing['anim']
    elif thing['type'] == HOPPER: # 29
        args[2] = 14 + thing['dir'] + 2 * thing['subtype']
        args[3] = thing['status'] / 4.2
    elif thing['type'] == CARRIER: # 30
        args[2] = 11
        args[3] = 3 + thing['anim']
    elif thing['type'] == SPIKEBALL: # 28
        args[2] = 11
        args[3] = thing['anim']
    elif thing['type'] == ANTIFECTLING: # 49
        args[2] = 7
        args[3] = 17
    elif thing['type'] == INFECTLING: # 15
        args[2] = 5
        args[3] = 13 + thing['subtype']

def drawTiles(file, w, h, infront):
    tl = 0; tr = w / SPR_SIZE; tt = 0; tb = h / SPR_SIZE
    x = tl
    while x <= tr:
        y = tt
        while y <= tb:
            if tiles[x][y][0] > 0 and (tiles[x][y][1] > 0) == infront:
                drawSprite((x-tl) * SPR_SIZE, (y - tt) * SPR_SIZE, (tiles[x][y][0] - 1) % 24, (tiles[x][y][0] - 1) / 24, 1, 1, tilesheet, lv_img)
                drawSprite((x-tl) * SPR_SIZE, (y - tt) * SPR_SIZE, tiles[x][y][2] % 24, 22 - (2 if see_through(tiles[x][y][0]) else 0) + tiles[x][y][2] / 24, 1, 1, tilesheet, lv_img)
                pass
            y = y + 1
        x = x + 1

def imagecopy (dst_im, src_im, dst_x, dst_y, src_x, src_y, src_w, src_h):
    src_im_crop = src_im.crop((src_x, src_y, src_x + src_w, src_y + src_h))
    dst_im.paste(src_im_crop, (dst_x, dst_y), src_im_crop)
    return True

def makeBeret (type, subtype, x, y, dir):
    args = 6 * [None]
    beret = {}
    beret['type'] = type;
    beret['subtype'] = subtype;
    beret['x'] = x;
    beret['y'] = y;
    beret['startx'] = x;
    beret['starty'] = y;
    beret['stickx'] = 0;
    beret['sticky'] = 0;
    beret['vx'] = 0;
    beret['vy'] = 0;
    beret['jump'] = 0;
    beret['dead'] = 0;
    beret['timer'] = -1;
    beret['status'] = 0;
    beret['link'] = -1;
    beret['islinked'] = -1;
    beret['anim'] = 0;
    beret['telething'] = 0;
    beret['icy'] = 0;
    beret['gravmod'] = DOWN;
    beret['floating'] = 0;
    beret['solid'] = 1;
    beret['nomove'] = 1;
    beret['nohmove'] = 0;
    beret['novmove'] = 0;
    beret['infront'] = 0;
    beret['inback'] = 0;
    beret['width'] = 20;
    beret['height'] = 24;
    beret['pickup'] = 0;
    beret['dir'] = dir;
    beret['crashspeed'] = -1;
    beret['animate'] = 1;
    drawThing(beret, args)
    drawSprite(args[0], args[1], args[2], args[3], args[4], args[5], spritesheet, lv_img)

def readLevel(file):
    if os.path.basename(file) == 'rooms':
        begindata = levelfile.read(1)
        if begindata == '': return None
        lv_lcode = ord(struct.unpack('<c', begindata)[0])
        lv_acode = ord(struct.unpack('<c', levelfile.read(1))[0])
    else:
        lv_lcode = None
        lv_acode = None
    begindata = levelfile.read(4)
    if begindata == '': return None
    lv_bkgtemp = checkint(struct.unpack('<I', begindata)[0])
    lv_musictemp = checkint(struct.unpack('<I', levelfile.read(4))[0])
    lv_width = checkint(struct.unpack('<I', levelfile.read(4))[0])
    lv_height = checkint(struct.unpack('<I', levelfile.read(4))[0])
    lv_startx = checkint(struct.unpack('<I', levelfile.read(4))[0])
    lv_starty = checkint(struct.unpack('<I', levelfile.read(4))[0])
    lv_things = [{} for i in range(THINGMAX)]
    for i in range(THINGMAX):
        lv_things[i]['x'] = struct.unpack('<f', levelfile.read(4))[0]
        lv_things[i]['y'] = struct.unpack('<f', levelfile.read(4))[0]
        lv_things[i]['vx'] = struct.unpack('<f', levelfile.read(4))[0]
        lv_things[i]['vy'] = struct.unpack('<f', levelfile.read(4))[0]
        lv_things[i]['stickx'] = struct.unpack('<f', levelfile.read(4))[0]
        lv_things[i]['sticky'] = struct.unpack('<f', levelfile.read(4))[0]
        lv_things[i]['startx'] = struct.unpack('<f', levelfile.read(4))[0]
        lv_things[i]['starty'] = struct.unpack('<f', levelfile.read(4))[0]
        lv_things[i]['speed'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['jump'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['jumpdelay'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['teledelay'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['width'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['height'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['dir'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['timer'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['status'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['link'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['islinked'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['anim'] = struct.unpack('<f', levelfile.read(4))[0]
        lv_things[i]['telething'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['ontele'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['dead'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['collide'] = 4 * [None]
        lv_things[i]['collide'][0] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['collide'][1] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['collide'][2] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['collide'][3] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['colltarget'] = 4 * [None]
        lv_things[i]['colltarget'][0] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['colltarget'][1] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['colltarget'][2] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['colltarget'][3] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['type'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['subtype'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['pickup'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['icy'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['gravmod'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['floating'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['solid'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['deathtouch'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['crashspeed'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['nomove'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['nohmove'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['novmove'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['infront'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['inback'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
        lv_things[i]['animate'] = checkint(struct.unpack('<I', levelfile.read(4))[0])
    x = 0
    while x < lv_width / SPR_SIZE:
        y = 0
        while y < lv_height / SPR_SIZE:
            z = 0
            while z < 3:
                tiles[x][y][z] = checkint(struct.unpack('<I', levelfile.read(4))[0])
                z = z + 1
            y = y + 1
        x = x + 1
    return {
        'lv_width':lv_width,
        'lv_height':lv_height,
        'lv_startx':lv_startx,
        'lv_starty':lv_starty,
        'lv_things':lv_things,
        'lv_lcode':lv_lcode,
        'lv_acode':lv_acode
    }

def see_through(type):
    return (type >= WHITEFUZZSEE and type <= GREENFUZZSEE) or type == GLASS or (type >= PURPLETILESEE and type <= WHITETILESEE) or type == CLEAR or type == REDFUZZSEE or type == BLACKTILESEE or (type >= WHITESPRSEE and type <= PURPLESPRSEE) or (type >= REDSEECRPT and type <= BLUESEECRPT) or type == ANTITILE or (type >= SPIKEU and type <= SPIKEL);

for arg in sys.argv[1:]:
    if arg == "--beret": opt['drawBeret'] = True
    if arg == "--noberet": opt['drawBeret'] = False
    process = True
    if os.path.isfile(arg) == False: process = False
    if not 'room' in arg and not 'meta' in arg: process = False
    if process == True:
        lv_dir = os.path.dirname(arg)
        if os.path.exists(lv_dir + '/images/') == False: os.mkdir(lv_dir + '/images/')
        lv_pngdir = lv_dir + '/images/'
        if '.' in os.path.basename(arg):
            lv_filename = os.path.basename(arg[0:arg.find('.', -5)])
            lv_meta = arg[0:arg.find('.',  -5)+1]+'meta'
            lv_room = arg[0:arg.find('.', -5)+1]+'room'
        else:
            lv_filename = os.path.basename(arg)
            lv_meta = os.path.dirname(arg)+'/metas'
            lv_room = os.path.dirname(arg)+'/rooms'

        spritesheet = Image.open(os.path.dirname(lv_dir) + '/images/spritesheet.png')
        tilesheet = Image.open(os.path.dirname(lv_dir) + '/images/tilesheet.png')

        tiles = [[[None] * 3 for i in range(LVLMAX)] for i in range(LVLMAX)]
        levelfile = open(lv_room, 'rb')
        while True:
            lv_data = readLevel(lv_room)
            if lv_data == None: break
            lv_things = lv_data['lv_things']
            lv_img = Image.new('RGB', (lv_data['lv_width'], lv_data['lv_height']), '#FFFFFF')
            drawTiles(lv_room, lv_data['lv_width'], lv_data['lv_height'], 0)
            drawSprites(-1)
            drawSprites(0)
            if opt['drawBeret']: makeBeret(BERET, istophat, lv_data['lv_startx'], lv_data['lv_starty'], 1)
            drawSprites(1)
            drawTiles(lv_room, lv_data['lv_width'], lv_data['lv_height'], 1)
            if lv_filename != 'rooms':
                lv_img.save(lv_pngdir + lv_filename + '.png')
            else:
                lv_img.save(lv_pngdir + lv_filename + '.' + str(lv_data['lv_lcode']) + '-' + str(lv_data['lv_acode']) + '.png')
